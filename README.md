# Terraformed AWS development server

There is a public AWS image (AMI ami-611ecc01) ready to go as a personal version of the code server.

The AMI will change in the future with updates to the image. 

You can find the image in community AMIs by searching for 2pisoftware.



The image is based on Ubuntu 16.04

The image includes

- docker
- python3
- aws command line tools aws cli
- aws eb command line tools  eb-cli 
- terraform
- jq json parser
- cmfive_deploy and cmfive-dev_deploy repositories installed in opt with images prebuilt so build cache works.
- script /buildcmfiveimages.sh that will pull, build, tag and push updates to the base cmfive and cmfive-dev images
- automatic shutdown at 1am by editing /etc/crontab inside the image
- 2G swapfile to cope with hungry builds

You can use this image to create a new AWS instance using the web console.

You can also create the instance using the AWS CLI tools and optionally Terraform from the command line.

!! This image is for region us-west-2 (oregon). To use in other regions you must copy the image.

## Initialise

- Create an AWS access key. 
- [Install AWS CLI tools](http://docs.aws.amazon.com/cli/latest/userguide/installing.html)
- Configure region and access key with ```aws configure```. Ensure output format is json
- [Install jq (json parser)](https://stedolan.github.io/jq/download/)   or ```apt-get install jq```
- Create keys for SSH.  You will need the generated key file to login to your instance using ssh -i or putty.
```
KEY_NAME=terraform
aws ec2 --region us-west-2 create-key-pair --key-name $KEY_NAME | jq -r ".KeyMaterial" > $KEY_NAME.pem
```

### DNS Delegation
Once you have started an instance (see below) you can optionally map your own domain name with an A record to the public IP of the instance.

If you want to be able to start docker containers on arbitrary sub domains  delegate BOTH *.yourdomain.com and @.yourdomain.com to the instance.


### For working with AWS CLI tools

1. Update the default security group to allow inbound web and ssh access.

3. Create instance with 
```
aws ec2 run-instances --image-id ami-611ecc01 --count 1 --instance-type t2.micro --key-name $KEY_NAME --security-groups default
```


### For working with Terraform

- Install Terraform https://www.terraform.io/downloads.html. Unzip and put the binary somewhere on your path.
- Clone the terraform_codeserver repository 
```
git clone https://steve_ryan@bitbucket.org/2pisoftware/terraform_codeserver.git
```
Configure region and access keys and ssh key file with terraform plan
	```terraform plan -out terraform.tfvars```

To create EC2 instances and their dependencies:

	```terraform apply```

To destroy all:

	```terraform destroy```

!! Currently the terraform configuration creates a load balancer, only one instance and security groups.

## Login

To login to your instance use username ububntu and the $KEY_NAME.pem file (may need to use puttygen to convert ppk) 


## Management

### Automatic shutdown

Edit the crontab file to enable.
```
# UNCOMMENT ME TO AUTO SHUTDOWN AT 1AM
1 1     * * *   root    #/sbin/shutdown -f now
```

### Build cmfive and cmfive-dev images and push to repository

To be able to push to code.2pisoftware.com:5000 you must first login
```
docker login code.2pisoftware.com:5000
```
Once logged in, the script /bin/buildcmfiveimages.sh  will build and push the latest versions of the cmfive and cmfive-dev images.

[See here](https://github.com/2pisoftware/docker-cmfive/blob/master/2pisoftware_continuous_integration.md#maintain-user-access-to-the-registry) to add a user .


## LINKS

[Similar Example](http://awsadvent.tumblr.com/post/105835590469/aws-advent-2014-using-terraform-to-build)

[Using tags with access controls]http://blogs.aws.amazon.com/security/post/Tx29HCT3ABL7LP3/Resource-level-Permissions-for-EC2-Controlling-Management-Access-on-Specific-Ins
