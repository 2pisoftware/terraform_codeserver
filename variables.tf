variable "key_name" {
    description = "Name of the SSH keypair to use in AWS."
    default = "syn"
}

variable "key_path" {
    description = "Path to the private portion of the SSH key specified."
    default = "syn.pem"
}
variable "instance_type" {
    default = "t2.micro"
}
variable "name" {
    default = "codeserver"
}
variable "description" {
    default = "2pi code server"
}


variable "access_key" {}
variable "secret_key" {}

variable "aws_region" {
    description = "AWS region to launch servers."
    default = "us-west-2"
}

# 2Pi Code Server
variable "aws_amis" {
    default = {
        us-west-2 = "ami-611ecc01"
    }
}
